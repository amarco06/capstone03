import React, { useState, useContext } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link, useHistory } from 'react-router-dom';
import { SidebarData } from './SidebarData';
import {
	MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
	MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon, MDBTypography
} from "mdbreact";
import './Navbar.css';
import { IconContext } from 'react-icons';
import UserContext from '../../../UserContext'

function Navigation() {

	const [sidebar, setSidebar] = useState(false);
	const [isOpen, setIsOpen] = useState(false);
	const showSidebar = () => setSidebar(!sidebar);

	const history = useHistory();

	const { user, setUser, unsetUser } = useContext(UserContext);

	const logout = () => {
		unsetUser();
		setUser({ accessToken: null });
		history.push('/login')
	}

	function toggleCollapse() {
		setIsOpen(!isOpen)
	}

	function navItems() {
		if (!user.isLogin) {
			return (
				<>
					<MDBDropdownItem as={Link} href="/register">Register</MDBDropdownItem>
					<MDBDropdownItem as={Link} href="/login">Login</MDBDropdownItem>
				</>
			)
		} else {
			return <MDBDropdownItem as={Link} onClick={logout}>Logout</MDBDropdownItem>
		}
	}
	return (
		<>
			<IconContext.Provider value={{ color: '#fff' }}>
				<MDBNavbar color="default-color" dark expand="md">
					<FaIcons.FaBars onClick={showSidebar} className='menu-bars' />
					<MDBNavbarBrand className="mx-4">
						<strong className="white-text">DY Cafe</strong>
					</MDBNavbarBrand>
					<MDBNavbarToggler onClick={toggleCollapse} />
					<MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
						<MDBNavbarNav className="mr-5 align-items-center" right>
							<MDBNavItem>
								<MDBNavLink className="waves-effect waves-light mx-3" to="/users/info">
									{user.isLogin && <MDBTypography className="m-0" tag='h5'>Hi, <strong>{user.firstName}</strong></MDBTypography>}
								</MDBNavLink>
							</MDBNavItem>

							{
							user.isLogin &&
								<MDBNavItem>
									<MDBNavLink className="waves-effect waves-light mx-3" to="/cart">
										<MDBIcon size="lg" fas icon="shopping-cart" />
									</MDBNavLink>
								</MDBNavItem>
							}

							<MDBNavItem>
								<MDBDropdown>
									<MDBDropdownToggle nav caret >
										<MDBIcon size="lg" icon="user" />
									</MDBDropdownToggle>
									<MDBDropdownMenu className="dropdown-default dropdown-menu-left">
										{navItems()}
									</MDBDropdownMenu>
								</MDBDropdown>
							</MDBNavItem>
						</MDBNavbarNav>
					</MDBCollapse>
				</MDBNavbar>

				<nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
					<ul className='nav-menu-items' onClick={showSidebar}>
						<li className='navbar-toggle'>
							<Link to='#' className='menu-bars'>
								<AiIcons.AiOutlineClose />
							</Link>
						</li>
						{SidebarData.filter((item) =>
							(item.path == '/user/purchases') ? user.isLogin : true
						).map((item, index) => {
							return (
								<li key={index} className={item.cName}>
									<Link to={item.path}>
										{item.icon}
										<span className="title">{item.title}</span>
									</Link>
								</li>
							);
						})}
					</ul>
				</nav>
			</IconContext.Provider>
		</>
	);
}

export default Navigation;