
import React, { useState, useEffect, useContext } from 'react'
import { MDBContainer, MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBRow, MDBCol, MDBIcon } from 'mdbreact';


import Swal from 'sweetalert2';
import { Redirect, useHistory, Link } from 'react-router-dom';
import AddProductModal from './AddProductModal';
import DeleteProductModal from './DeleteProductModal';

import UserContext from '../../UserContext';
import * as FaIcons from 'react-icons/fa';
import './product.css';

export default function Products() {
    const history = useHistory();

    const [modalShow, setModalShow] = React.useState(false);
    const [deleteModalShow, setDeleteModalShow] = React.useState(false);
    const [selectedProduct, setSelectedProduct] = React.useState();


    const { user } = useContext(UserContext);
    const [productList, setProductList] = useState([]);

    function showAddModal() {
        setSelectedProduct(null)
        setModalShow(true)
    }
    function showDeleteModal(productId) {
        setSelectedProduct(productId)
        setDeleteModalShow(true)
    }

    function showUpdateModal(product) {
        setSelectedProduct(product)
        setModalShow(true)
    }
    function addToCart(productId) {
        fetch(`https://api-dycafe.herokuapp.com/users/create-order`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`,
            },
            body: JSON.stringify({
                productId: productId,
                quantity: 1
            })
        })
            .then(res => res.json())
            .then(data => {
                Swal.fire({
                    icon: 'success',
                    text: 'Added to cart!'
                })
                    .then(() => history.go(0))
            })
    }

    useEffect(() => {
        fetch('https://api-dycafe.herokuapp.com/products/active-products', {
            Authorization: `Bearer ${user.accessToken}`
        })
            .then(res => res.json())
            .then(data => {
                let actionButton = ''
                if (user.isAdmin) {
                    actionButton = (
                        <div>
                            <FaIcons.FaEdit className="mx-2" />
                            <FaIcons.FaTrash className="mx-2" />
                        </div>
                    )
                }
                setProductList(data.map(product => {
                    return (
                        <MDBCol sm="12" md="4">
                            <MDBCard className="m-4">
                                <MDBCardImage
                                    top
                                    src={product.image}
                                    overlay='white-slight'
                                    hover
                                    waves
                                    alt='MDBCard image cap'
                                    className="card-height"
                                />
                                <MDBCardBody>
                                    <div className="d-flex justify-content-between">
                                        <MDBCardTitle>
                                            <Link to={'/products/' + product._id}>{product.name}</Link>
                                            <h3 className="my-4">{product.price} PHP</h3>
                                        </MDBCardTitle>
                                        {
                                            user.isAdmin &&
                                            <div>
                                                <FaIcons.FaEdit className="mx-2" onClick={() => showUpdateModal(product)} />
                                                <FaIcons.FaTrash className="mx-2" onClick={() => showDeleteModal(product._id)} />
                                            </div>
                                        }
                                    </div>
                                    <MDBCardText>
                                        {product.description}
                                    </MDBCardText>
                                    {user.isLogin &&
                                        <>
                                            <hr />
                                            <MDBBtn onClick={() => addToCart(product._id)}>Add to Cart</MDBBtn>
                                        </>
                                    }
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    )
                }))
            })
    }, [])

    return (
        <>
            <MDBContainer>
                {
                    user.isAdmin &&
                    <div className="d-flex flex-row-reverse">
                        <MDBBtn onClick={() => showAddModal()}>Add Product</MDBBtn>
                    </div>
                }

                <AddProductModal
                    show={modalShow}
                    product={selectedProduct}
                    onHide={() => setModalShow(false)} />


                <DeleteProductModal
                    show={deleteModalShow}
                    product={selectedProduct}
                    onHide={() => setDeleteModalShow(false)} />
                <MDBRow>
                    {productList}
                </MDBRow>
            </MDBContainer>
        </>
    )
}