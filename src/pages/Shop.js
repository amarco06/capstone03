import React from 'react';
import Banner from '../components/base/Banner'
import { Container } from 'react-bootstrap'
import Products from '../components/shop/Products';

export default function Shop() {
    return (
        <>
            <div className="my-5">
                <Banner currentPath="shop" pathname="/shop"/>
                <Products />
            </div>
        </>
    )
}