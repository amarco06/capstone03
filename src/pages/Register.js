import React, { useState, useEffect, useContext } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import validator from 'validator'
import * as FaIcons from "react-icons/fa";
import * as GoIcons from "react-icons/go";
import { RiLockFill } from "react-icons/ri";
import UserContext from '../UserContext';

const Register = () => {
    //https://i.pinimg.com/originals/94/09/7e/94097e458fbb22184941be57aaab2c8f.png
    const { user } = useContext(UserContext);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [contactNo, setContactNo] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [registerButton, setRegisterButton] = useState(false);

    const history = useHistory();

    useEffect(() => {

        if ((email && password && verifyPassword) && (password === verifyPassword) && (validator.isEmail(email))) {
            setRegisterButton(true);
        } else {
            setRegisterButton(false);
        }
    }, [email, password, verifyPassword]);

    function registerUser(e) {
        e.preventDefault();

        fetch('https://api-dycafe.herokuapp.com/users/register', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                contactNo: contactNo,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                Swal.fire({
                    title: 'Great!',
                    icon: 'success',
                    text: 'You are now registered!'
                });
                if (data) {
                    history.push('/login');
                } else {
                    history.push('/');
                }
            })
    }

    if (user.accessToken !== null) {
        return <Redirect to="/" />
    }

    return (
        <MDBContainer className="my-5">
            <MDBCard>
                <MDBCardBody>
                    <MDBRow>
                        <MDBCol md="6">
                            <form className="p-5" onSubmit={(e) => registerUser(e)}>
                                <p className="h4 text-center py-4">Sign up</p>
                                <div className="grey-text">
                                    <MDBInput
                                        label="Your first name"
                                        icon="user"
                                        group
                                        type="text"
                                        validate
                                        error="wrong"
                                        success="right"
                                        onChange={e => setFirstName(e.target.value)}
                                        required
                                    />
                                    <MDBInput
                                        label="Your last name"
                                        icon="user"
                                        group
                                        type="text"
                                        validate
                                        error="wrong"
                                        success="right"
                                        onChange={e => setLastName(e.target.value)}
                                        required
                                    />
                                    <MDBInput
                                        label="Your email"
                                        icon="envelope"
                                        group
                                        type="email"
                                        validate
                                        error="wrong"
                                        success="right"
                                        onChange={e => setEmail(e.target.value)}
                                        required
                                    />
                                    <MDBInput
                                        label="Your contact"
                                        icon="mobile-alt"
                                        group
                                        type="number"
                                        validate
                                        error="wrong"
                                        success="right"
                                        required
                                        onChange={e => setContactNo(e.target.value)}
                                    />
                                    <MDBInput
                                        label="Your password"
                                        icon="lock"
                                        group
                                        type="password"
                                        validate
                                        onChange={e => setPassword(e.target.value)}
                                        required
                                    />
                                    <MDBInput
                                        label="Confirm your password"
                                        icon="exclamation-triangle"
                                        group
                                        type="password"
                                        validate
                                        error="wrong"
                                        success="right"
                                        onChange={e => setVerifyPassword(e.target.value)}
                                        required
                                    />
                                </div>
                                <div className="text-center py-4 mt-3">
                                    <MDBBtn disabled={!registerButton} type="submit">Register</MDBBtn>
                                </div>
                            </form>
                        </MDBCol>
                        <MDBCol md="6" className="align-self-center">
                            <img src="https://www.assertiv.com/static/All-Apps-Transparent-a302e9a5412dffaefb3072f0575a1b77.png" className="img-fluid" alt="" />
                        </MDBCol>
                    </MDBRow>
                </MDBCardBody>
            </MDBCard>

        </MDBContainer>
    );
};

export default Register;