import React from 'react';
import FeatureItems from '../components/home/FeatureItems'
import { Container } from 'react-bootstrap'
export default function Home() {
    return (
        <>
            <div className="my-5">
                <FeatureItems />
            </div>

        </>
    )
}